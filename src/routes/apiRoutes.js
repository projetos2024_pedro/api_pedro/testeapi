const router = require('express').Router()
const teacherController = require('../controller/teacherController');
const JSONPlaceholderController = require('../controller/JSONPlaceholderController');
const dbController = require('../controller/dbController');
const clienteController = require('../controller/clienteController');
const pizzariaController = require('../controller/pizzariaController');


router.get('/teacher/', teacherController.getTeacher);
router.post('/cadastroAluno/', teacherController.postAluno);
router.put('/cadastroAluno/', teacherController.updateAluno);
router.delete('/cadastroAluno/:id', teacherController.deleteAluno);

router.get("/external/", JSONPlaceholderController.getUsers);
router.get("/external/io", JSONPlaceholderController.getUsersWebSiteIO);
router.get("/external/com", JSONPlaceholderController.getUsersWebSiteCOM);
router.get("/external/net", JSONPlaceholderController.getUsersWebSiteNET);
router.get('/external/filter', JSONPlaceholderController.getCountDomain);

router.get("/pizzaria/", dbController.getTables);
//router.get("/sistemaingressosonline/descusuario", dbController.descUsuario);

router.post("/pizzaria/cliente", clienteController.createCliente);
router.get("/pizzaria/cliente", clienteController.getAllClientes);
router.get("/pizzaria/cliente/2", clienteController.getAllClientes2);
router.get("/pizzaria/cliente/3", clienteController.getAllClientes3);

router.get("/pizzaria/pedido", pizzariaController.listarPedidosPizzas);
router.get("/pizzaria/pedido/join", pizzariaController.listarPedidosPizzasJoin);

module.exports = router