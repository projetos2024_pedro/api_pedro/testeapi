const connect = require("../db/connect");

module.exports = class dbController {
  /*Consulta para obter a lista de tabelas (show tables) */
  static async getTables(req, res) {
    const queryShowTables = "show tables";

    connect.query(queryShowTables, async function (err, result, fields) {
        if(err){
            console.log("Erro: " + err);
            return res.status(500).json({error: "Erro ao obter tabelas da database"});
        }

        /* Extrai os nomes da tabela de forma organizada */
        const tableNames = result.map(row => row[fields[0].name]);

        //res.status(200).json({message: "Tabelas do banco - forma bruta: ", result, tables: tableNames})
        console.log("Tabelas do banco de dados: ", tableNames);

        //Organização e descrição das tabelas do banco
        const tables = [];

        //Iterar sobre os resultados para obter a descrição dee cada tabela
        for(let i = 0; i < result.length; i++){

            //Analisando o banco através de seus atributos
            const tableName = result[i] 
            [`Tables_in_${connect.config.connectionConfig.database}`];

            //Acionando o comando desc
            const queryDescTable = `describe ${tableName} `;

            try{
                const tableDescription = await new Promise((resolve, reject) => {
                    connect.query(queryDescTable, function (err, result, fields){
                        if(err){
                            reject(err);
                        }
                        resolve(result);
                    })
                }); //Fim da const

                tables.push({
                    name: tableName,
                    description: tableDescription
                });
            }catch (error) {
                console.log(error);
                return res.status(500).json({error: "Erro ao obter a descrição da tabela!"});
            }

        };

        res.status(200).json({message: "Obtendo todas as tabelas de suas descrições", tables})
        
    });

    
  } /*fim getTables */

  /*Consulta para obter a descrição da tabela usuário */
  /*
  static async descUsuario(req, res) {
    const queryDescUsuario = "desc usuario";

    connect.query(queryDescUsuario, async function (err, result, fields) {
        if(err){
            console.log("Erro: " + err);
            return res.status(500).json({error: "Erro ao obter a descrição da tabela usuário"});
        }

        res.status(200).json({message: "Descrição da tabela usuário", result});
    });

  }
  */
}; /*fim class */
